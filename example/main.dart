import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:stream_subscription_builder/stream_subscription_builder.dart';

void main() {
  final controller = StreamController<String>();
  final stream = controller.stream;

  // Create builder instance
  StreamSubscriptionBuilder(
      () => [
            // Subscribe to stream. Subscription will be disposed when StreamSubscriptionBuilder is disposed.
            stream.listen((data) => print("Stream data: $data")),
          ],
      child: Container());
}
