import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:stream_subscription_builder/stream_subscription_builder.dart';

void main() {
  testWidgets('should subscribe and close subscription on dispose',
      (WidgetTester tester) async {
    // Given
    final subscribeFunction = _SubscribeFunction<String>();
    final subscription = _MockedSubscription<String>();
    when(subscribeFunction.call()).thenReturn(subscription);
    final builder = StreamSubscriptionBuilder(
      () => [
        subscribeFunction(),
      ],
      child: Container(),
    );

    // When
    await tester.pumpWidget(builder);

    // Then
    verify(subscribeFunction.call());

    // And
    await tester.pumpWidget(Container());

    // Then
    verify(subscription.cancel());
  });

  testWidgets('should build with child', (WidgetTester tester) async {
    // Given
    final builder = StreamSubscriptionBuilder(
      () => [],
      child: Container(),
    );

    // When
    await tester.pumpWidget(builder);

    // Then
    expect(find.byType(Container), findsOneWidget);
  });
}

class _SubscribeFunction<T> extends Mock {
  StreamSubscription<T> call();
}

class _MockedSubscription<T> extends Mock implements StreamSubscription<T> {
  StreamSubscription<T> initTest() => this;
}
