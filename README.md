# stream_subscription_builder

A package to use and auto-close multiple stream subscriptions in flutter.

## Usage

Use `StreamSubscriptionBuilder` to subscribe and auto-dispose multiple subscriptions (`StreamSubscription`).

```dart
StreamSubscriptionBuilder(
    () => [
        bloc.stream().listen((event) => event.use((data) => print("Stream data: $data"))),
    ],
    child: Text("Child"),
);
```

## Samples

You can experiment with sample application hosted here: https://gitlab.com/marcin.jelenski/bloc-showcase

Also `test` directory contains all `stream_subscription_builder` use cases.