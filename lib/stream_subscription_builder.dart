library stream_subscription_builder;

import 'dart:async';

import 'package:flutter/widgets.dart';

/// Builds a list of subscriptions.
/// All subscriptions will be cancelled when the [SubscriptionsBuilder] widget no longer exists.
///
/// Example:
///
/// ```dart
/// SubscriptionsBuilder builder = () => [
///    bloc.stream1().listen((data) => print("Stream 1 data: $data")),
///    bloc.stream2().listen((data) => print("Stream 2 data: $data")),
/// ];
/// ```
typedef List<StreamSubscription> SubscriptionsBuilder();

/// {@template stream_subscription_builder}
/// Builds a widget with a [child] and a list of subscriptions should be closed when widget is disposed.
/// {@endtemplate}
class StreamSubscriptionBuilder extends StatefulWidget {
  /// A child, will be put directly inside this [StreamSubscriptionBuilder].
  final Widget child;
  final SubscriptionsBuilder _subscriptionsBuilder;

  /// {@macro stream_subscription_builder}
  const StreamSubscriptionBuilder(this._subscriptionsBuilder,
      {Key key, @required this.child})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _StreamSubscriptionBuilderState();
  }
}

class _StreamSubscriptionBuilderState extends State<StreamSubscriptionBuilder> {
  final List<StreamSubscription> _streamSubscriptions = [];

  _StreamSubscriptionBuilderState();

  @override
  Widget build(BuildContext context) {
    if (widget._subscriptionsBuilder != null) {
      final list = widget._subscriptionsBuilder();
      list.forEach(_streamSubscriptions.add);
    }
    return widget.child;
  }

  @override
  void dispose() {
    for (final subscription in _streamSubscriptions) {
      subscription.cancel();
    }
    _streamSubscriptions.clear();
    super.dispose();
  }
}
