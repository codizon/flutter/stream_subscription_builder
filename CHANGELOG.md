## [0.1.0+4] - 8.12.2020

* Update repository url.

## [0.1.0+3] - 27.07.2019

* Repair stateful widgets subscriptions.

## [0.1.0+2] - 27.07.2019

* Fix flutter rebuild.

## [0.1.0+1] - 1.12.2019

* Initial release.
